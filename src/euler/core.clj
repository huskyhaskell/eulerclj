(ns euler.core)

;; Multiples of 3 and 5
(def problem1
  (let [below-1000 (range 1000)
              filtered-3-or-5 (filter #(or (= (mod % 3) 0) (= (mod % 5) 0)) below-1000)]
          (reduce + filtered-3-or-5)))

;; Even Fibonacci numbers
(defn fib
  "Compute nth fibonacci number, starting from 1 and 2."
  ([n a b]
   (if (<= n 1)
     a
     (fib (- n 1) b (+ a b))))
  ([n]
   (fib n 1 2)))

(defn fib-range
  "Compute a vector of numbers in the fibonacci sequence not exceeding n."
  [n]
  (loop [fibs []
         c 1]
    (let [next-fib (fib c)]
      (if (> next-fib n)
        fibs
        (recur (conj fibs next-fib) (+ c 1))))))

(def problem2
  (let [ceiling 4000000]
          (reduce + (filter even? (fib-range ceiling)))))

;; Largest prime factor
;; Very slow solution
(defn prime?
  "Compute a number's primeness."
  [n]
  (let [bigN (BigInteger/valueOf n)
        certainty 5]
    (.isProbablePrime bigN certainty)))
  
(defn primes
  "Compute n primes into vector."
  [n]
  (into [] (take n (filter prime? (drop 2 (range))))))

(defn primes-up-to
  "Compute primes upto n into vector."
  [n]
  (into [] (take-while #(<= % n) (filter prime? (drop 2 (range))))))

(defn largest-prime-factor
  "Compute the largest prime factor of n."
  [n]
  (let [primes-to-check (primes-up-to (Math/sqrt n))]
    (last (filter #(= (mod n %) 0) primes-to-check))))
    
(def problem3
  (largest-prime-factor 600851475143))

;; Largest palindrome product
(defn palindromic?
  "Compute palindromeness of a number."
  [n]
  (let [number-to-string (into () (str n))
        reversed-number-to-string (reverse number-to-string)]
    (= number-to-string reversed-number-to-string)))
  
(defn palindromic-products
  "Compute products of numbers from and to a certain
  number which are palindromic."
  [from to]
  (let [products-with-factors (map #(list % (reduce * %)) (for [n1 (range from (+ to 1))
                                                                n2 (range from (+ to 1))]
                                                            (vector n1 n2)))]
    (filter #(palindromic? (nth % 1)) products-with-factors)))

(defn largest-palindromic-product
  "Compute factors which produce
  largest product in range from to."
  [from to]
  (let [pal-prods (palindromic-products from to)]
    (first (sort-by #(nth % 1) > pal-prods))))

(def problem4
  (largest-palindromic-product 100 999))
 
;; Smallest multiple
(defn gcd'
  "Compute gcd of two numbers."
  [a b]
  (if (zero? b)
    a
    (gcd' b (mod a b))))

(defn lcm'
  "Compute lcm of two numbers."
  [a b]
  (/ (Math/abs (* a b)) (gcd' a b)))

(defn least-common-from-to
  "Compute the smallest positive integer
  that can be evenly divided in the range of
  from to."
  [from to]
  (loop [result 1
         n from]
    (if (= n to)
      result
      (recur (lcm' result n) (+ n 1)))))

(def problem5
  (least-common-from-to 1 20))

;; Sum square difference
(defn sum-of-squares
  "Compute the sum of squares in the 
  range of from to."
  [from to]
  (reduce + (map #(* % %) (range from (+ to 1)))))

(defn square-of-sum
  "Compute the square of the sum in the
  range of from to."
  [from to]
  (let [sum (reduce + (range from (+ to 1)))]
    (* sum sum)))

(defn diff-square-sum
  "Compute the difference between the sum of squares
  and the square of the sum in range of from to."
  [from to]
  (- (square-of-sum from to)
     (sum-of-squares from to)))

(def problem6
  (diff-square-sum 1 100))

;; 10001st prime
(def problem7
  (let [num 10001]
    (last (primes num))))

;; Largest product in a series
(def large-number
  7316717653133062491922511967442657474235534919493496983520312774506326239578318016984801869478851843858615607891129494954595017379583319528532088055111254069874715852386305071569329096329522744304355766896648950445244523161731856403098711121722383113622298934233803081353362766142828064444866452387493035890729629049156044077239071381051585930796086670172427121883998797908792274921901699720888093776657273330010533678812202354218097512545405947522435258490771167055601360483958644670632441572215539753697817977846174064955149290862569321978468622482839722413756570560574902614079729686524145351004748216637048440319989000889524345065854122758866688116427171479924442928230863465674813919123162824586178664583591245665294765456828489128831426076900422421902267105562632111110937054421750694165896040807198403850962455444362981230987879927244284909188845801561660979191338754992005240636899125607176060588611646710940507754100225698315520005593572972571636269561882670428252483600823257530420752963450)

(defn number->list
  "Turn a number into a list."
  [n]
  (map #(Character/digit % 10) (str n)))

(defn partition-into-consecutives
  "Partition a number n into consecutive partitions of size
  partition-size."
  [partition-size n]
  (loop [number-list (number->list n)
         result []
         i (count number-list)]
    (if (< i partition-size)
      result
      (recur (rest number-list) (cons (take partition-size number-list) result) (- i 1)))))

(def problem8
  (apply max (map #(reduce * %) (partition-into-13s large-number))))
  
;; Special Pythagorean triplet
(defn pythagorean-triples-range
  "Compute maps of pythagorean triples in the range of n."
  [n]
  (for [m (range 2 (+ n 1))
        n (range 1 n)
        :when (< n m)]
    (let [a (- (* m m) (* n n))
          b (* 2 (* m n))
          c (+ (* m m) (* n n))]
      {:a a :b b :c c :sum (+ a b c)})))

(def problem9
  (reduce
   *
   (take 3
         (vals
          (first (filter #(= (:sum %) 1000) (pythagorean-triples-range 1000)))))))

;; Summation of primes
;; Brute force
(def problem10
  (reduce + (primes-up-to (- 2000000 1))))

;; Largest product in a grid
(def grid [[8 2 22 97 38 15 0 40 0 75 4 5 7 78 52 12 50 77 91 8]
           [49 49 99 40 17 81 18 57 60 87 17 40 98 43 69 48 4 56 62 0]
           [81 49 31 73 55 79 14 29 93 71 40 67 53 88 30 3 49 13 36 65]
           [52 70 95 23 4 60 11 42 69 24 68 56 1 32 56 71 37 2 36 91]
           [22 31 16 71 51 67 63 89 41 92 36 54 22 40 40 28 66 33 13 80]
           [24 47 32 60 99 03 45 2 44 75 33 53 78 36 84 20 35 17 12 50]
           [32 98 81 28 64 23 67 10 26 38 40 67 59 54 70 66 18 38 64 70]
           [67 26 20 68 2 62 12 20 95 63 94 39 63 8 40 91 66 49 94 21]
           [24 55 58 5 66 73 99 26 97 17 78 78 96 83 14 88 34 89 63 72]
           [21 36 23 9 75 0 76 44 20 45 35 14 0 61 33 97 34 31 33 95]
           [78 17 53 28 22 75 31 67 15 94 3 80 4 62 16 14 9 53 56 92]
           [16 39 5 42 96 35 31 47 55 58 88 24 0 17 54 24 36 29 85 57]
           [86 56 0 48 35 71 89 7 5 44 44 37 44 60 21 58 51 54 17 58]
           [19 80 81 68 5 94 47 69 28 73 92 13 86 52 17 77 4 89 55 40]
           [4 52 8 83 97 35 99 16 7 97 57 32 16 26 26 79 33 27 98 66]
           [88 36 68 87 57 62 20 72 3 46 33 67 46 55 12 32 63 93 53 69]
           [4 42 16 73 38 25 39 11 24 94 72 18 8 46 29 32 40 62 76 36]
           [20 69 36 41 72 30 23 88 34 62 99 69 82 67 59 85 74 4 36 16]
           [20 73 35 29 78 31 90 1 74 31 49 71 48 86 81 16 23 57 5 54]
           [1 70 54 71 83 51 54 69 16 92 33 48 61 43 52 1 89 19 67 48]])


(defn map-partition-cons
  [f partition-size coll]
  (loop [c coll
         result []
         i (count coll)]
    (if (< i partition-size)
      result
      (recur (rest c) (cons (reduce f (take partition-size c)) result) (- i 1)))))

(defn max-prod-grid
  [grid]
  (apply max (map (comp
                   (partial apply max)
                   (partial map-partition-cons * 4)) grid)))

(defn transpose-grid
  [grid]
  (apply mapv vector grid))

(defn diagonalize-grid
  [start dimensions grid]
  (loop [result []
         [row & rst] grid
         i start]
    (if (= i dimensions)
      result
      (recur (cons (nth row i) result) rst (+ i 1)))))

(defn collect-diagonal
  [dimensions grid]
  (loop [i 0
        result []]
    (if (= i dimensions)
      result
      (recur (+ i 1) (cons (diagonalize-grid i dimensions grid) result)))))
